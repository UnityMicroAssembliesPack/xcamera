﻿using UnityEngine;

namespace CameraTypes
{
    //NB: This is class (not structure) for possibility to
    // reference value from several place
    public struct CameraSettings
    {
        public CameraSettings(Camera inCamera = null) {
            position = new Vector3();
            rotation = new Quaternion();
            fieldOfView = 60.0f;

            if (inCamera) {
                setFromCamera(inCamera);
            }
        }

        public void setFromCamera(Camera inCamera) {
            XUtils.check(inCamera);

            position = inCamera.transform.position;
            rotation = inCamera.transform.rotation;
            fieldOfView = inCamera.fieldOfView;
        }

        public void applyForCamera(Camera inCamera) {
            XUtils.check(inCamera);

            inCamera.transform.position = position;
            inCamera.transform.rotation = rotation;
            inCamera.fieldOfView = fieldOfView;
        }

        public Ray getViewRay() {
            return new Ray(
                position, XMath.getDiractionFromRotation(rotation)
            );
        }

        public override bool Equals(object inObject) {
            if (!(inObject is CameraSettings)) return false;

            var theObject = (CameraSettings)inObject;
            if (position != theObject.position) return false;
            if (rotation != theObject.rotation) return false;
            if (fieldOfView != theObject.fieldOfView) return false;
            return true;
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }

        public static CameraSettings lerp(
            CameraSettings inA, CameraSettings inB, float inProgress)
        {
            CameraSettings theResult = new CameraSettings();

            theResult.position =
                Vector3.Lerp(inA.position, inB.position, inProgress);
            theResult.rotation =
                Quaternion.Lerp(inA.rotation, inB.rotation, inProgress);
            theResult.fieldOfView =
                Mathf.Lerp(inA.fieldOfView, inB.fieldOfView, inProgress);

            return theResult;
        }

        public Vector3 position;
        public Quaternion rotation;
        public float fieldOfView;
    }
}
