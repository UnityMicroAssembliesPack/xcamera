﻿using UnityEngine;

using CameraTypes;
using BoxedCameraSettings = Box<CameraTypes.CameraSettings>;

public class CameraManager : MonoBehaviour
{
    //Methods
    //-API
    public void setCamera(CameraSettings inCameraSettings) {
        XUtils.check(inCameraSettings);
        
        internalStopTransition();
        internalSetCameraSettings(inCameraSettings);
    }

    //NB: Speed is used for ortho size change
    //TODO: Find some better approach for transition time setup
    public void setCamera(BoxedCameraSettings inCameraSettings, float inTransitionSpeed) {
        XUtils.check(inCameraSettings);
        XUtils.check(inTransitionSpeed >= 0.0f);
        
        internalStopTransition();
        
        if (0.0f == inTransitionSpeed) {
            internalSetCameraSettings(inCameraSettings);
        } else {
            internalStartTransition(
                inCameraSettings, inTransitionSpeed
            );
        }
    }

    //-Implementation
    private void Awake() {
        XUtils.check(_camera);

        _cameraSettings = new BoxedCameraSettings(
            new CameraSettings(_camera)
        );
    }

    private void FixedUpdate() {
        validateState();
        
        if (isTransiting()) {
            updateTransition();
        } else {
            updateCameraFromSettings();
        }
    }

    //--Camera settings
    private void internalSetCameraSettings(CameraSettings inCameraSettings) {
        //_cameraSettings.value = inCameraSettings;
    }

    private void updateCameraFromSettings() {
        _cameraSettings.value.applyForCamera(_camera);
    }

    //--Transition
    private void internalStartTransition(
        BoxedCameraSettings inTargetCameraSettings, float inSpeed)
    {
        _transitionState = new Optional<TransitionState>(
            new TransitionState(inTargetCameraSettings, inSpeed)
        );
        _cameraSettings = null;
    }

    private void updateTransition() {

        //Compute progress based on main param value & speed
        float theTransitionSpeed = _transitionState.value.speed;
        CameraSettings theTargetCameraSettings = _transitionState.value.targetCameraSettings;

        float theTransitionSpeedPerUpdate = theTransitionSpeed * Time.fixedUnscaledDeltaTime;

        float theProgressForSpeedPerUpdate = theTransitionSpeedPerUpdate /
            (theTargetCameraSettings.position - _camera.transform.position).magnitude;
        theProgressForSpeedPerUpdate = Mathf.Clamp(theProgressForSpeedPerUpdate, 0.0f, 1.0f);

        //Stop transition if params will be achieved on this update or...
        if (1.0f == theProgressForSpeedPerUpdate) {
            internalStopTransition();
            internalSetCameraSettings(theTargetCameraSettings);
        } else {
            var theCurrentCameraSettings = new CameraSettings(_camera);

            CameraSettings theTransitionSettings = CameraSettings.lerp(
                theCurrentCameraSettings, theTargetCameraSettings,
                theProgressForSpeedPerUpdate
            );

            theCurrentCameraSettings.applyForCamera(_camera);
        }
    }

    private void internalStopTransition() {
        _transitionState = new Optional<TransitionState>();
    }

    //--Utility accessors
    private bool isTransiting() {
        return null == _cameraSettings;
    }

    //--Debug
    private void validateState() {
        XUtils.check(_camera);
        XUtils.check(Vector3.one == _camera.transform.localScale);

        XUtils.check(_transitionState.isSet() || null != _cameraSettings);
    }

    //Fields
    [SerializeField] private Camera _camera = null;

    //-Runtime
    private BoxedCameraSettings _cameraSettings = null;

    private struct TransitionState {
        public TransitionState(BoxedCameraSettings inTargetCameraSettings, float inSpeed)
        {
            targetCameraSettings = inTargetCameraSettings;
            speed = inSpeed;
        }

        public BoxedCameraSettings targetCameraSettings;
        public float speed;
    }
    private Optional<TransitionState> _transitionState;
}
