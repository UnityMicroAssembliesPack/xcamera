﻿using UnityEngine;

using CameraTypes;
using BoxedCameraSettings = Box<CameraTypes.CameraSettings>;

#if UNITY_EDITOR
[ExecuteAlways]
#endif
public class CameraSettingsHolder : MonoBehaviour
{
    //Methods
    //-API
    public BoxedCameraSettings getSettings() {
        return _cameraSettings;
    }

    public void setSettings(CameraSettings inCameraSettings) {
        _cameraSettings.value = inCameraSettings;

#       if UNITY_EDITOR
        _cameraSettings.value.applyForCamera(_camera);
#       endif
    }

    //-Implementation
#   if UNITY_EDITOR
    private void Awake() {
        _camera = XUtils.getComponent<Camera>(
            this, XUtils.AccessPolicy.ShouldExist
        );
    }

    private void Update() {
        if (!Application.isEditor) return;

        _cameraSettings.value.setFromCamera(_camera);
    }
#   endif

    //Fields
    private Camera _camera = null;
    private BoxedCameraSettings _cameraSettings =
        new BoxedCameraSettings(new CameraSettings());
}
